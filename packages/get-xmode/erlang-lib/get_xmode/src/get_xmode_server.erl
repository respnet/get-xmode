-module(get_xmode_server).

-behaviour(gen_server).

%% API
-export([
         start_link/0
         %% get_xmode/0
        ]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-include_lib("econfd/include/econfd.hrl").
-include_lib("econfd/include/econfd_errors.hrl").

-define(SERVER, ?MODULE).

-record(state, {daemon}).

%%%===================================================================
%%% API
%%%===================================================================

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%% get_xmode() ->
%%     call(get_xmode).
%%
%% call(Msg) ->
%%     gen_server:call(?SERVER, Msg, infinity).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
init([]) ->
    process_flag(trap_exit, true), % Triggers call to terminate/2
    State = register_callbacks(?CONFD_SILENT),
    log(info, "Server started", []),
    {ok, State}.

%%--------------------------------------------------------------------
%% handle_call(ping, _From, State) ->
%%     Reply = pong,
%%     {reply, Reply, State};
handle_call(Req, _From, State) ->
    log(error, "Got unexpected call: ~p", [Req]),
    Reply = error,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
handle_cast(Msg, State) ->
    log(error, "Got unexpected cast: ~p", [Msg]),
    {noreply, State}.

%%--------------------------------------------------------------------
handle_info(Info, State) ->
    log(error, "Got unexpected info: ~p", [Info]),
    {noreply, State}.

%%--------------------------------------------------------------------
terminate(Reason, _State) ->
    log(info, "Server stopped - ~p", [Reason]),
    ok.

%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

register_callbacks(DebugLevel) ->
    ActionCbs = #confd_action_cb{actionpoint = 'get-xmode',
                                 action      = fun get_xmode/4},
    Host = {127,0,0,1},
    Port = ?NCS_PORT,
    DaemonName = list_to_atom(lists:concat([?MODULE, "_action"])),
    {ok, Daemon} = econfd:init_daemon(DaemonName, DebugLevel, user,
                                      {}, Host, Port),
    ok = econfd:register_action_cb(Daemon,ActionCbs),
    ok = econfd:register_done(Daemon),

    #state{daemon=Daemon}.

get_xmode(_, _, _, [{[Ns|tid], Input}]) ->
    log(trace, "get_xmode: ~p", [Input]),
    % Input contains {12,42} which is {?C_UINT32, 42}. We only need to grab the value (42).
    Tid = element(2, Input),
    log(trace, "tid: ~p", [Tid]),
    try
        Th = ncs_util:tid2th(Tid),
        Mode = cs_trans:get_xmode(Th),
        log(trace, "mode: ~p", [Mode]),
        {ok, [{[Ns|xmode], atom_to_binary(Mode, utf8)}]}
    catch notid ->
        log(trace, "notid", []),
        {ok, [{[Ns|xmode], <<"notid">>}]}
    end.

log(error, Format, Args) ->
    econfd:log(?CONFD_LEVEL_ERROR, "~p: " ++ Format, [?SERVER | Args]);
log(info, Format, Args) ->
    econfd:log(?CONFD_LEVEL_INFO,  "~p: " ++ Format, [?SERVER | Args]);
log(trace, Format, Args) ->
    econfd:log(?CONFD_LEVEL_TRACE, "~p: " ++ Format, [?SERVER | Args]).
